let fruits = [
  "🍇",
  "🍍",
  "🥝",
  "🍋",
  "🍑",
  "🫒 ",
  "🍓",
  "🍇",
  "🥭",
  "🍍",
  "🥝",
  "🍋",
  "🍑",
  "🫒 ",
  "🍓",
  "🥭",
];

fruits.sort(() => Math.random() - 0.5);

let box_container = document.querySelector("#tiles_container");
let boxes = box_container.querySelectorAll(".tile");
let time = document.querySelector(".time");
let timerRunning = false;
let move_count = 0;
let sec = 0;
let leftBox = 16;
let firstFlip, secondFlip;

fruits.map((item, index) => {
  boxes[index].querySelector(".tile_front").innerText = fruits[index];
});

let start_button = document.querySelector(".start");
start_button.addEventListener("click", start_game);

function flipTile() {
  this.classList.toggle("flip");
  startTimer();
  moveCount();

  if (!firstFlip) {
    firstFlip = this;
    firstFlip.removeEventListener("click", flipTile);
  } else if (!secondFlip) {
    secondFlip = this;
    secondFlip.removeEventListener("click", flipTile);

    if (firstFlip.children[0].innerText == secondFlip.children[0].innerText) {
      firstFlip = false;
      secondFlip = false;
      leftBox--;
      leftBox--;

      if (leftBox == 0) {
        document.querySelector(".win_message").innerText =
          "Congratulations!! You Won!!";
      }
    } else {
      setTimeout(() => {
        firstFlip.addEventListener("click", flipTile);
        secondFlip.addEventListener("click", flipTile);
        firstFlip.classList.toggle("flip");
        secondFlip.classList.toggle("flip");
        firstFlip = false;
        secondFlip = false;
      }, 500);
    }
  }
}

function start_game(e) {
  document.querySelector(".start").style.color = "gray";
  boxes.forEach((tile) => tile.classList.toggle("flip"));
  setTimeout(() => {
    boxes.forEach((tile) => tile.classList.toggle("flip"));
  }, 1500);
  start_button.removeEventListener("click", start_game);
  boxes.forEach((tile) => tile.addEventListener("click", flipTile));
}

function startTimer() {
  if (!timerRunning) {
    setInterval(updateTimer, 1000);
    timerRunning = true;
  }
}

function updateTimer() {
  if (leftBox !== 0) {
    sec++;
    time.innerText = sec;
  }
}

function moveCount() {
  if (leftBox !== 0) {
    move_count++;
    document.querySelector(".move").innerText = move_count;
  }
}
